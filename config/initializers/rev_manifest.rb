REV_MANIFEST = {content:nil}
class MANIFEST_LOADER
  def self.execute
    rev_manifest_path = 'public/assets/rev-manifest.json'
    if File.exist?(rev_manifest_path)
      REV_MANIFEST[:content]= JSON.parse(File.read(rev_manifest_path))
    end
  end
end

MANIFEST_LOADER.execute

