module ApplicationHelper

  def gulp_asset_path(path)

    if Rails.env.development?
      reload_manifest
    end


    if (defined?(REV_MANIFEST) and REV_MANIFEST[:content].present?)
     new_path = REV_MANIFEST[:content][path]
    end

    raise "path miss match error: #{path}" if new_path.blank?
    return "/assets/javascripts/#{new_path}" if new_path.end_with?('.js')
    "/assets/stylesheets/#{new_path}"        if new_path.end_with?('.css')

  end

  def reload_manifest
    MANIFEST_LOADER.execute
  end

end
