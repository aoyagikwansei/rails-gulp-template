var gulp = require('gulp');


var runSequence = require('run-sequence');

//
// 最小化なし 開発時
//
gulp.task('default', function(){
    runSequence(
        'clean',
        ['compile-sass','compile-scss'],
        'webpack'
    );
});

//
// 最小化あり デプロイ時
//
gulp.task('production', function(){
    runSequence(
        'clean',
        ['compile-sass','compile-scss'],
        'webpack-production'
    );
});